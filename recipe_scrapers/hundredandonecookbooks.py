from ._abstract import AbstractScraper
from ._utils import get_minutes, normalize_string


class HundredAndOneCookbooks(AbstractScraper):

    @classmethod
    def host(self):
        return '101cookbooks.com'

    def title(self):
        return self.soup.find('h1').get_text()

    def hasrecipe(self):
        return not(self.soup.find('div',{'id':'recipe'}) is None)

    def total_time(self):
        try:
            return get_minutes(self.soup.find('span', {'class': 'preptime'}))
        except:
            return ''

    def ingredients(self):
        try:
            ingredients_html = self.soup.find('div', {'id': 'recipe'}).find('blockquote').find('p')
            return ingredients_html.get_text().split('\n')
        except:
            return ['']

    def instructions(self):
        try:
            instructions_html = self.soup.find('div', {'id': 'recipe'}).find('blockquote').find_next_siblings()

            return '\n'.join([
                normalize_string(instruction.get_text())
                for instruction in instructions_html
            ])
        except:
            return ''
